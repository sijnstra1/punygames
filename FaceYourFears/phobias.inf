!% -~S
!% $OMIT_UNUSED_ROUTINES=1

! The very first lines of the main source code file for a game can
! contain compiler options, like the two lines above. -~S disables
! strict error checking. This is otherwise used in z5 and z8 games by
! default. While useful for debugging, it adds ~10 KB to the story file
! size and it makes the game slower.
! $OMIT_UNUSED_ROUTINES=1 makes the compiler remove all routines which
! aren't used. This can save some space.
! inform6 -v6 +lib advent.inf
! Pho Bia

Constant Story      "Face your fears";
Constant Headline   "^An exploration of finding your your way by seeking out all your phobias. And finding a cure through a really bad pun.^
A PunyJam #2 game, (c) Shawn Sijnstra 2021.^For credits, simply type ~credits~.^";

Array UUID_ARRAY string "UUID://1EFCCB4F-BBA6-4570-BA73-84085AFB5492//";
#Ifdef UUID_ARRAY;#Endif;

Release 1;
Serial "211121";

! Uncomment ONE of the two following lines, to show either time or score/turns
! Leaving both commented out makes the library bigger.
!Constant STATUSLINE_TIME; Statusline time;
Constant STATUSLINE_SCORE; Statusline score;

! Comment out to keep track of score
! The value is what will be shown as the score on statusline in z3
! Constant NO_SCORE = 0;

! Customize the statusline in z5+ (will have no effect in z3)
!Constant OPTIONAL_SL_NO_SCORE;
!Constant OPTIONAL_SL_NO_MOVES;

! Uncomment to add optional features to PunyInform
Constant DEBUG;
!Constant CUSTOM_ABBREVIATIONS;
!Constant CUSTOM_PLAYER_OBJECT = myPlayerObj;
Constant OPTIONAL_NO_DARKNESS;
!Constant OPTIONAL_ALLOW_WRITTEN_NUMBERS;
Constant OPTIONAL_EXTENDED_METAVERBS;
Constant OPTIONAL_EXTENDED_VERBSET;
!Constant OPTIONAL_PRINT_SCENERY_CONTENTS;
Constant OPTIONAL_SCORED;
!Constant OPTIONAL_FULL_SCORE; ! Comment out NO_SCORE when uncommenting this
!Constant OPTIONAL_FULL_DIRECTIONS;
!Constant OPTIONAL_SIMPLE_DOORS;
!Constant OPTIONAL_SHIP_DIRECTIONS;
!Constant OPTIONAL_GUESS_MISSING_NOUN;
!Constant OPTIONAL_MANUAL_SCOPE;
!Constant OPTIONAL_MANUAL_REACTIVE;
!Constant OPTIONAL_ORDERED_TIMERS;
!Constant OPTIONAL_PROVIDE_UNDO;
!Constant OPTIONAL_REACTIVE_PARSE_NAME;
!Constant RUNTIME_ERRORS = 0; ! 0, 1 or 2. 0 = smallest file, 2 = most info

#Ifv5;
Constant OPTIONAL_PROVIDE_UNDO;
#Endif;

! Define any library constants you need here, like MAX_SCORE, AMUSING_PROVIDED,
! MAX_CARRIED, SACK_OBJECT,  etc.

Constant MAX_SCORE = 13;
Constant INITIAL_LOCATION_VALUE = Field;
Constant OBJECT_SCORE = 5;
Constant ROOM_SCORE = 7;

Include "globals.h";

! Define your own global variables here, if any

Global fauxconvo = false;
Global fearfactor = 0;
Global ladderstate = 1;
Global rejection = 0;
Global boxclaus = 0;
Global sinkdone = 0;
Global lucky13 = 0;
Global tidyconvo = 0;
Global TakenCoffee = 0;
Global TakenTea = 0;
Global TakenBix = 0;
Global GotBeer = 0;

! Define the entry point routines you need here, like Amusing, DarkToDark etc.

! Uncomment to add PunyLib extensions
!Include "ext_menu.h";
!Include "ext_flags.h";
!Include "ext_quote_box.h";
!Include "ext_cheap_scenery.h";

[PrintRank;
	print ".";
	if (lucky13 == 0 ) {print " Out of 13? Thirteen! Your triskaidekaphobia kicks in. Ouch.";score++;lucky13=1;};
	print "^";
];

[ BeforeParsing;
  if ((TakenCoffee == 1 ) && (location == kitchen) && (coffee notin kitchen) && (coffee notin player))
      for (wn = 1 ::)
          switch (NextWordStopped()) {
              'coffee': (parse+wn*4-6)-->0 = 'coxxee';
               'beans': (parse+wn*4-6)-->0 = 'bxxns';
				-1: return;
          }
  if ((TakenTea == 1 ) && (location == kitchen) && (tea notin kitchen) && (tea notin player))
      for (wn = 1 ::)
          switch (NextWordStopped()) {
              'tea': (parse+wn*4-6)-->0 = 'txa';
              'leaves': (parse+wn*4-6)-->0 = 'lxxves';
              -1: return;
          }
  if ((TakenBix == 1 ) && (location == kitchen) && (biscuits notin kitchen) && (biscuits notin player))
      for (wn = 1 ::)
          switch (NextWordStopped()) {
              'biscuits': (parse+wn*4-6)-->0 = 'biscuxxs';
			  'biscuit': (parse+wn*4-6)-->0 = 'biscuxx';
              -1: return;
          }
];

Include "puny.h";

! Uncomment to add PunyLib extensions
!Include "ext_waittime.h";

[  CookSub; if (player notin Kitchen) {"How do you plan to cook without a kitchen?";} else {"You're not sure how to cook that.";}; ];
Verb 'cook' * noun -> Cook;

Verb 'exam' = 'examine';
Verb 'inspect' = 'examine';

[ MakeSub; if (noun == "")
	{"I think you meant to ~make something~. Please try again.";}
		else {print(The) noun," ",(IsOrAre) noun," already in existence here.";rtrue;};];
Verb 'make'
 * noun -> Make;
 
 [ WashSub; if (noun == "")
	{"I think you meant to ~wash something~. Please try again.";}
		else {
		switch (noun) {
	'me', 'myself' : "You're prety clean already";
	default: "You don't need to wash that.";
			};
		};
	];
Verb 'wash'
 * noun -> Wash;
 
 [ UseSub; if (noun == "")
	{"I think you meant to ~use something~. Please try again.";}
		else {
		"You don't need to use that.";
			};
	];
Verb 'use'
 * noun -> Use;

 [ FlushSub; if (noun == "")
	{"I think you meant to ~flush something~. Please try again.";}
		else {
		"You don't need to flush that.";
			};
	];
Verb 'flush'
 * noun -> Flush;

[ CreditsSub; "Many thanks to Fredrik Ramsberg for his part in convincing me to experience the other side of the interpreter and take up PunyInform,
	and to Garry Francis for providing much needed feedback on my first complete adventure."; ];
Verb 'credits' * -> Credits;

Extend only 'look'
  * 'around' -> Look;

Verb 'talk'
	*	'to'/'with'	creature	-> Talk;

[TalkSub;
	if (noun == player) "The conversation is hardly inspiring.";
	if (RunLife(noun,##Talk) ~= false) return;
	"You might want to try asking about a specific topic.";
];


Object Field "Grassy Field"
	with
		description [;
		print "You are standing in a small grassy field, nestled between rolling hills. The area feels extremely open and exposed. A path leads to the south, and
		there is a shaded area to the east. ";
		switch (fearfactor) {
			0	: print "It seems a nice enough place but something is not quite right. You may want to look around.";
			1	:	print "Something makes you want to keep looking around even though you kind of don't want to.";
			2	:	print "Okay so we might need to get out of here soon. Maybe one last look.";
			3	:  	score=score+2;
					print "Turns out you really have found a fear of open spaces. Agoraphobia is doing you no favours right now.";
			4	:	print "You really shouldn't be here any more.";
			5	:	fearfactor--;
					print "You really shouldn't be here any more.";
			};
			fearfactor++; print "^";
			],
		s_to Path,
		e_to Shade;
		
Object -> Field2 "field"
	with name 'field',
		description "Looking at the field is a lot more comforting than simply looking around.",
		has scenery static;

Object -> Hills "hills"
	with name "hill" "hills",
	description "The hills provide some comfort against the open field.",
	has scenery static pluralname;

Object Shade "Shady Spot"
	with
		description [; print "You are among some trees in the shade, feeling comfortable and relaxed. There's a 
		number of shrubs scattered around here. The field to the west makes you less comfortable.";
			switch (fearfactor) {
				1 : print " You are not keen to spend too much time in that field and yet...^";
				2   : print " That field is really creeping you out.^";
				3,4,5,6  : print " You're staying here now. You don't regret challinging yourself in the field so much as coming here afterwards.^";}],
		before [;
			Listen: "You hear what sounds like a hungry cat coming from the large shrub.";
			],
		w_to [;
			if (fearfactor <3 ) {return Field;} else {print "You aren't going back to the field today.^^"; return Shade;}]
		;

Object -> Shade2 "shade"
	with name 'shade' 'shady' 'spot',
		description "The shade provides some comfort from the stark open field.",
		has scenery static;


Object -> Shrub "rather large shrub"
	with name 'shrub' 'shrubs' 'shrubbery',
		description "There's one particularly large shrub that catches your attention. You think you hear a noise coming from it.",
		before [;
			Search: "You can just make out a hungry cat hiding in there.";
			Attack: "That seems ill-advised. There are noises coming from the shrub.";
			ThrownAt:
				if (noun == meat) {
					move meat to shrub; print "You toss the meat at the shrub, which results in a rather mangy
					cat popping out to thank you. You didn't even know you had ailurophobia until now.^";score++;rtrue;};
				],
	has scenery static;

Object -> Noise "noise from shrub"
	with name 'noise',
		description "(Listening) The noise appears to be coming from one particular shrub.",
		before [;
			Listen : "It sounds a lot like a hungry cat.";
			ThrownAt:
				if (noun == meat) {
					move meat to shrub; print "You toss the meat at the shrub you heard the noises from, which results in a rather mangy
					cat popping out to thank you. You didn't even know you had ailurophobia until now.^";score++;rtrue;};
				],
	has scenery static;

Object -> Trees "tall trees"
	with name 'trees' 'tree',
		description "The trees here are large and provide you with some shade.",
	has scenery static pluralname;

Object Path "Winding Path"
	with
		description [; print "The path winds around some trees, providing shelter and comfort from the open field. 
		The field is to the north, and there is a well-kept small pub to the south. There's also a narrow dirt path that leads around the pub to the west.";
		if (fearfactor>2) {print " No way you're going back to that field today.";}; print "^";],
		n_to [;
			if (fearfactor <3 ) {return Field;} else {print "No chance you are going back to that field. Not any more. Not today. If there was anything else you needed to do there, it won't happen today.^"; return Path;}],
		s_to Pub,
		w_to PubSide,
		e_to "The garden is way too overgrown that way.";

Object -> Path2 "winding path"
	with name 'path',
		description "The main path is pleasant enough and winds it way from the field in the north to the pub in the south. The minor dirt path is functional more than anything.",
		has	scenery static;

Object -> Pub2 "small public bar"
	with name 'pub' 'public',
		description "It's a small pub that is well presented and quite inviting. You imagine a helpful and friendly barman works there.",
		has scenery static;

Object Pub "Small Public Bar"
	with
		description "You are standing in a small bar room, which has a wood-panelled counter and a small number of beer taps, as well as a small range of other drinks.
		There is a barman standing behind the bar, occasionally wiping things down with a damp towel. There is a diner to the east, a bathroom to the west, and the exit back north.",
		n_to Path,
		w_to Loo,
		s_to "The barman won't let you go behind the bar.",
		e_to Diner;
		
Object -> Taps "beer taps"
	with
		name 'taps' 'tap',
		description "The beer taps are largely unfamiliar and not at all enticing. You're going to need some advice here.",
		before [;
			Take: "The barman looks at you sternly while you attempt to take the beer taps as a souvenir. Those taps aren't going anywhere.";
		],
	has pluralname static;

Object -> Bar "bar"
	with
		name 'bar' 'counter',
			description "The bar counter itself is rather nice. While the range of drinks isn't that great, I'm sure the barman can talk you through what is there if you ask him about the drinks.",
		has scenery;

Object -> Barman "barman"
	with
		name 'barman' 'barkeep' 'rob' 'publican',
		description "The barman looks well travelled in the ways of life, and like he knows a thing or two about what's being served here. Perhaps he could help he could help you.",
		life [tempvar w j;
		tempvar=second;
!         Answer: "The barman looks up towards you, raises an eyebrow and asks ~Yes?~";
		 if(((second == 'faux') || (second == 'faux2' ) || (second == 'fauxyxxy') || (second == 'real'))&& fauxconvo == false) 
			{tempvar = 'real';
			fauxconvo = true;};
		if(tempvar == 'fauxyzzy' && score <7) {tempvar = 'faux';};
		if(tempvar == 'faux' && score > 6) {tempvar = 'fauxyzzy';};
	     Ask: switch (tempvar)
         {
          'beer', 'taps' : "The barman looks at the beer taps and says, ~Yes we have a range of beers on tap here. They
		  are all good. Perhaps you need a taste of one.~";
		  'barman', 'himself', 'self', 'yourself', 'publican', 'you', 'him'	: "~What is there to say? What do you want to know? It's a solitary existince for me now here in this pub.
					As a small bar publican, I am happy to tell you anything I know about these parts. 
					Sure it does get lonely here but sometimes I like that. By the way, you can call me Rob.~";
		  'rob'	: "~What is there to say? What do you want to know? It's a solitary existince for me now here in this pub. As a small bar publican, I am happy to tell you anything I know about these parts. 
					Sure it does get lonely here but sometimes I like that.~";
		  'wine', 'spirits', 'whiskey', 'whisky', 'scotch', 'rum', 'gin', 'brandy', 'tequila', 'vodka', 'drink' : "~While we have a great selection,
		  you strike me more as a beer person. I'd certainly recommend beer as our specialty.~";
		  'coffee', 'tea' : "~I'm so sorry - the hot water is broken so hot beverages are out today. I would recommend anything else from the bar.~";
		  'cocktail', 'cocktails' : "~You have an expensive taste for someone without any money on you.~";
		  'promotional', 'promotion' : "~I know I mentioned promotional, but you can just ask about the beer.~";
		  'field'		: "~That field up there is very open. Many look around and find it intimidating. Persistence is the key.~";
		  'ladder'		: "~Got to get up and repair the roof from time to time, or simply enjoy the view. Persistence is the key.~";
		  'repair'		: "~I generally take care of the roof repairs. Gives me a great view across the landscape.~";
		  'roof', 'view'		: "~I definitely recommend the view from the roof. Unless you are afraid of heights.~";
		  'condiments', 'seasoning', 'mustard', 'sauce' : "~Not all condiments are created equal. Same goes for seasoning.~";
		  'meat'		: "~We use high quality meats in our meals. If I were hungry and someone threw one of those at me, I'd be a happy man if you know what I mean.~";
		  'cat', 'cats'	: "~There are stories of some stray cats about the place. They're probably just looking for scraps of food. They won't cause you much trouble, unless you have a fear of cats.~";
		  'm4zvm'		: "~If you like playing PunyInform adventures, and have access to a TRS-80 model 4, M4ZVM is a great way to enjoy them!~";
		  'punyinform'	: "~From what I understand, it's a lightweight library for making Inform adventure games; aimed at producing small, retro-computing size game files. It's written by some really great people. You should check it out if you get the chance.~";
		  'pepper'		: "~Pepper, while useful, is never as useful as salt.~";
		  'salt'		: "~Halophobia is one inconvenient fear.~";
		  'key'		: "~Life has many keys. Persistence is one of them.~";
		  'bar', 'counter', 'pub'		: "~I quite like this bar. I like to keep everything looking good here.~ You can hear the pride in his voice.";
		  'tools', 'tool', 'gardening'	: "~I take care of the gardening myself using the tools in the shed. I don't need any help with that. Some say it's a taking point about this pub.~"; 
		  'persistent', 'persistence' :	"~There's a lot to life that can be unlocked with some persistence. Third time's a charm.~";
		  'bathroom', 'sink' : if (tidyconvo == 0) {tidyconvo=1;}; "~Not much I can say there except that we haven't had much luck getting people to stay around to keep that part of the place tidy. Open to suggestions.~";
		  'tidy'	: if (tidyconvo == 0) {tidyconvo=1;}; "~To be honest, that bathroom needs a really good clean with some cleaning supplies. I'm understating when I say tidy.~";
		  'clean', 'cleaning', 'supplies', 'scrubbing'	: if (tidyconvo==1) {move CleanGear to player;update_moved = true;tidyconvo++ ;"The barman says ~You did mention the bathroom ealier. Thank you for volunteering to help!~ as he hands you some cleaning supplies.";}
					else {"~I do generally like things better when they are clean. As a public bar, we are certainly equipped with cleaning supplies. Feel free to ask again if you notice anything of concern.~";};
		  'shed'	: "~The shed is around the back. Feel free to investigate if you wish.~";
		  'box'		: "~That box is surprisingly large. I can't remember what it is for but it's big enough that you could fit someone in there if they are't too afraid of small spaces. 
					It may be an optical illusion but many say it is smaller on the inside if you look.~";
		  'kitchen', 'diner', 'meal', 'food', "meals"	: "~The kitchen is open from 6 to 8 pm, Wednesday to Sunday nights. Feel free to investigate the dining room and inspect the kitche.~";
		  'snack', 'snacks', 'chips', 'crisps' : "~Snacks aren't good for you. You're better off with real food or beer.~";
          'drinks', 'other' : "~Look... I'm not sure what you're into but we've got everything here.~";
		  'recommend', 'favourite', 'favorite', 'recommendation' : "~I like them all. you'll really need to taste all of them to know your favourite.~";
		  'taste', 'taster', 'sample', 'sampler' : "~Normally I would give away tasters but we're pretty low on everything. I recommend you buy a pint.~";
		  'pint' : "~Are you sure you have enough money for a pint? All of these cost money. Except the promotional beer but I'm not sure it's a real beer.~";
		  'real', 'fake' : "~You definitely can't afford a real beer. You should try to buy a faux beer.~";
		  'life', 'love', 'loss', 'lone', 'lonely', 'loneliness' :  if (rejection==0) {rejection++;score++;};
			"The barman leans over the bar, beckoning you closer, then tells tales about life, love and loss.
			Sadness and eremophobia kicks in.";
		  'faux' : "You ask for a faux beer in your most confident voice, but you are not yet ready to face your faux beer.";
		  'fauxyzzy' : if (GotBeer == 0) {move Fauxbeer to player; score++; GotBeer =1;
					"You take a deep breath. You are ready to face your greatest fauxbeer. The barman pours a pint and hands it to you.";}
					else {"~You've already been given a pint of faux beer. You should drink it when you are ready.~";};
			default : wn = consult_from;						
					  w = NextWord(); j = 0;
					  if(j > consult_words) "Sorry, can't help you with that!";
					  else { print "~I can't really help you with "; do { 
						print (InputWord) wn - 1," ";w = NextWord(); j++;} until (j == consult_words) ; print
						"right now.~^"; rtrue;};

!			default:
!  wn = consult_from;
!  w = NextWordStopped(); if(w == 'the') w = NextWordStopped();
!  if(w < 0) "Sorry, can't help you with that!";
!  wa = WordAddress(wn - 1); wl = WordLength(wn - 1);
!  print "No one can help you with ";
!  for(i=0 : i < wl : i++) print (char) wa->i;
!  "";

 ! wn = consult_from;
 ! w = NextWord(); j = 1; if(w == 'the') { w = NextWord(); j++; }
 ! if(j > consult_words) "Sorry, can't help you with that!";
 ! wa = WordAddress(wn - 1); wl = WordLength(wn - 1);
 ! print "No one can help you with ";
 ! for(i=0 : i < wl : i++) print (char) wa->i;
 !".";
!					  if(w == 'the') { w = NextWord(); j++; }

!  print "~I can't really help you with ",(InputWord) wn - 1," right now.~"
! [InputWord n wa wl i;
!  wa = WordAddress(n); wl = WordLength(n);
!  for(i=0 : i < wl : i++) print (char) wa->i;
!];
		 };
        ],		  
	has animate static;

Object PubSide "West Side of the Pub"
	with
		description "You are standing at the west side of the pub, on a narrow dirt path alongside the wall. This area isn't normally for the general public. It's not as well kept as the front.
		You can head back to the front of the pub by following the path north, or continue around to the back of the pub to the south.",
		n_to Path,
		s_to PubBack;

Object -> Ladder "long ladder"
	with name 'ladder',
		description "It's a long rickety ladder that you really don't trust. It looks like it's meant to get you to the roof.",
			before [;
			Climb, Use:	switch (ladderstate) {
				0	:   print "As tempting as it is to climb, do you really need to?^";
				1	:	print "I mean sure, you could climb the ladder, but it doesn't look in great condition.^";
				2	:	print "Those steps look not right but if you really insist I suppose you could climb it.^";
				3	:  	score=score+2;
						print "You brace yourself. As you climb the ladder you realise you are scared of heights. The bout of acrophobia makes you
						lose your footing, which causes you to break some of the rungs. You aren't climbing ladders again.^";
				4	:	print "Ladders are not your friend any more.^";
				5	:	ladderstate--;
						print "No, really - ladders are not your friend any more.^";
						}; ladderstate++; rtrue;
			Take:	"It's a long ladder that's been here even longer. It's not going anywhere.";
			],
		has static
		;

Object Loo "Unisex Bathroom"
	with
		description "You are in a malodourous bathroom. Something here hasn't been looked after in a while. You can return to the bar in the east.",
			before[;
			Smell: if (noun == 0) "The room and everything within smells more than you want it to. You can return to the bar in the east.";
			],
		e_to Pub;

Object -> Sink "sink",
	with name 'sink' 'basin',
	when_open "There is a sink here.",
	description [;print "A less than clean sink looms at you. While there is some soap on a rope hanging next to it, the contrast in hygiene is stark. ";
		  <<Search self>>;],
		before[;
		Take: "Like most sinks, it is bolted to the wall and isn't going anywhere. Unlike most sinks, it's really not clean. It makes you a little nervous.";
		Attack: "Why would you do that? You don't even want to touch it.";
		Touch, Wash, Rub: if (CleanGear in player) { if (sinkdone == 0) {
			score++; sinkdone=1;};
			"Even using the cleaning gear you regret touching the sink as your mysophobia kicks in. It's a little clearner than it was.";}
				else {"You aren't going near that sink without some serious cleaning supplies.";};
		Smell: "Ewww. This hasn't been dealt with in a while.";
		Use: if (sinkdone == 0) {"You get the feeling that using the sink would not make anything actually cleaner.";}
				else {"It's certainly better than it was, but you still don't think using the sink would make anything actually cleaner.";};
		],
		has static container open;

Object -> Soap "soap",
	with name 'soap',
		description "Just some ordinary soap on a rope. I don't think this will help you.",
		before [;
			Smell: "The faint hint of lavender doesn't cover up much of the general smell of the room.";
			Use: "While probably good for you, it doesn't solve the immediate problem of the sink.";
			Take: "It would be rude to take the only thing that is valiantly attempting to keep the room odour in check. You decide to leave it here to continue the good fight.";
			],
		has static scenery;

Object -> Toilet "toilet",
	with name 'loo' 'toilet',
	when_open "There is also a toilet here.",
	description [;print "A rather utilitarian object. ";<<Search self>>;],
		before[;
		Use: "You don't need to use the toilet right now.";
		Smell: "Everything smells pretty bad here. It's hard to tell where the smell comes from.";
		Flush: "Well that was a disappointing flush. Not much happened.";
		],
		has static container open;

Object Diner "Small Dining Area"
	with
		description "You are in a small dining area. There's a kitchen to the south, and the bar is to the west.",
		w_to Pub,
		s_to Kitchen;

Object -> Table "dining table"
	with
		name 'dining' 'table',
		description "It's standard small dining table that is neatly set out with standard cutlery and condiments in case a hungry customer arrives.",
		before[;
			Take: "Carrying a table around isn't going to help you.";
			Make: "The table is set neatly already.";
			Attack: "As you wish. You stub your toe on the table for no apparent reason.";
			],
		has static supporter;

Object -> Chairs "dining chairs"
	with name 'chair' 'chairs',
		description "It's a pair of dining chairs, matching the table.",
		before [;
			Take: "As lovely as these chairs are, I suspect the barman would notice if you tried to take them.";
			Push: "The chairs are already pushed in.";
			Attack: "You aren't a rock musician and this isn't an expensive hotel room.";
			],
		after [;
			Enter: "You sit down on one of the chairs.";
			Exit: "You get back up.";
		],
		has static supporter enterable pluralname;

Object -> Cutlery "cutlery"
	with
		name 'cutlery' 'knife' 'knives' 'fork' 'forks' 'spoon' 'spoons',
			description "The cutlery is laid out immaculately. A little too neat for such a small diner.",
			before[;
				Take: "You have enough problems today without disturbing a perfectly laid out table.";
				],
			has scenery static pluralname;

Object -> Condiments "condiments"
	with
		name 'condiments' 'seasoning' 'condiment',
			description "A rather standard fair of tomato sauce, mustard, brown sauce, along with salt and pepper to season.",
			has scenery pluralname;

Object -> Mustard "mustard"
	with name 'mustard',
		description "Just some ordinary mustard.",
			before[;
				Eat, Taste : "You taste a little. It's hot English mustard. Rather good recipe.";
				],
			after[;
				Take: if (player in diner) "You manage to grab the mustard without the barman noticing.";
				],
			has concealed pluralname;

Object -> Sauce "sauce"
	with name 'sauce' 'tomato' 'brown',
		description "Typical pub sauce bottle containing typical pub sauce.",
			before[;
				Eat, Taste, Use : "You taste a little bit of sauce. Not bad!";
				Take: "You hear the barman clear his throat. He's on to you. You leave it right where it is.";
				],
			has static scenery;

Object -> Pepper "pepper grinder"
	with name 'pepper' 'grinder',
		description "It's a pepper grinder. Surprisingly fancy for this pub.",
			before [;
				Eat, Taste, Use : "Confirmed. That grinder contains pepper.";
			],
			has concealed;

Object -> Salt "shaker of table salt"
	with
		name 'salt' 'shaker',
			description "A small salt shaker. Why would anyone make things more salty?",
			before[;
					Eat, Taste, Use : "You really don't want to do that. Perhaps keeping it with you will serve as a kind of charm.";
			],
			after[;
					Take : "Your halophobia takes you for a moment.";
				],
			has concealed scored;

Object Fauxbeer "faux beer"
!	has scored,
	with name 'beer' 'faux' 'faux beer',
	description "A pint of your finest phobias.",
	before[;
		Smell:	"It smells surprisingly good. With a hint of fear.";
		Drink: score++; deadflag=GS_WIN; "You take a deep breath and take a sip, feeling much better about yourself for facing all of your phobias.";
		];

Object Kitchen "Kitchen"
	with
		description "You are in a typical pub kitchen. The only exit is north, back to the dining area.",
			before[;
!							Cook:	"Sure this is a kitchen, but you've got better things to do right now.";
							Listen: if (Fridge has open) "You can hear the refrigerator beeping to remind you to close it.";
					],
		n_to Diner;

Object -> Fridge "refrigerator"
	with name 'fridge' 'refrigerator',
		description [;print "A plain white fridge. ";
			if (self has open) {"It's currently open, beeping to remind you to close it.";}
			else {"It's closed.";};],
		has openable container static;

Object -> -> meat "meat"
	with name 'meat',
		description "Some chunks of uncooked meat.",
		before[;
			Eat:	"I'm pretty sure that eating raw meat taken from an unfamiliar refrigerator is unlikely to result in anything positive.";
			Cook:	if (player notin Kitchen) {"Why would you try to cook the meat somewhere other than the kitchen?";} else
				{"You're not hungry and have better things to do.";};
			Smell:	"It's fresh enough. I'm sure it would make a good meal.";
			],
		has pluralname;

Object -> DryGoods "dry goods"
	with name 'goods' 'dry',
		description "Some dry goods are sitting here on the shelf. In many ways it's exactly what you would expect:
		there are labels allocating areas to the usual catering quantities of flour, spices, coffee, tea, sugar and biscuits.
		To be honest, I think some of these have been on the shelf way too long.",
		has static pluralname scenery;

Object -> Shelf "shelving"
	with name 'shelf' 'shelving' 'wire' 'rack',
		description "Some wire kitchen shelving, used for storing dry goods.",
		has static transparent pluralname supporter;

Object -> -> Flour "flour"
	with name 'flour',
		description "It's a large sack of flour, too large to cart around and very unexciting.",
			before [;
				Eat: "It's too dry to eat as is.";
				Cook, Use: "You don't really have the time nor all the ingredients to bake.";
				Take: "The sack is too large to take with you, and you don't want to carry loose flour around.";
				],
		has static pluralname scenery;

Object -> -> Spices "spices"
	with name 'spices' 'spice',
		description "A pretty boring collection. If this is anything to go by you are glad you aren't hungry.",
			before [;
				Take: "It's a set of spices that are too annoying to carry around with you. You don't need them.";
				Eat: "You taste the spices. There isn't much happening.";
				Cook, Use: "You don't really have the time nor all the ingredients to cook something.";
				Smell: "They don't smell very fresh.";
				],
		has static pluralname scenery;

Object -> -> Coffee "coffee beans"
	with name 'coffee' 'beans',
		description "Surprisingly good quality beans for pub coffee.",
			before [;
				Eat: "The coffee isn't made. It's just beans at this point.";
				Drink: "You'd need to make it first.";
				Smell: "The smell takes you away to Colombia. Smoooooth.";
				Make, Use: "You look around but there really isn't the equipment to make coffee. Perhaps you could ask for some drinks at the bar?";
				],
			after [;
				Take: if (location == kitchen) {TakenCoffee = 1 ; "You take some of the coffee beans with you.";};
				],
		has pluralname concealed;

Object -> -> Coxxee "coffee beans"
	with name 'coxxee' 'bxxns',
		description "Surprisingly good quality beans for pub coffee.",
			before [;
				Eat: "The coffee isn't made. It's just beans at this point.";
				Drink: "You'd need to make it first.";
				Smell: "The smell takes you away to Colombia. Smoooooth.";
				Make, Use: "You look around but there really isn't the equipment to make coffee. Perhaps you could ask for some drinks at the bar?";
				Take: "You've taken enough coffee beans from the kitchen.";
				],
		has pluralname scenery;

Object -> -> Tea "tea leaves"
	with name 'tea' 'leaves',
		description "Not the best box of tea leaves I've seen.",
			before [;
				Eat: "The tea isn't made. It's just leaves at this point.";
				Drink: "You'd need to make it first.";
				Smell: "The smell isn't very fresh and the tea seems to have been on the shelf for a while. Perhaps people drink mainly beer around here.";
				Make, Use : "You look around but there really isn't the equipment to make tea. Perhaps you could ask for some drinks at the bar?";
				],
			after [;
				Take: if (location == kitchen) {TakenTea = 1 ; "You take some of the tea leaves with you."; };
				],
		has pluralname concealed;

Object -> -> Txa "tea leaves"
	with name 'txa' 'lxxves',
		description "Not the best box of tea leaves I've seen.",
			before [;
				Eat: "The tea isn't made. It's just leaves at this point.";
				Drink: "You'd need to make it first.";
				Smell: "The smell isn't very fresh and the tea seems to have been on the shelf for a while. Perhaps people drink mainly beer around here.";
				Make, Use: "You look around but there really isn't the equipment to make tea. Perhaps you could ask for some drinks at the bar?";
				Take: "You have taken enough tea leaves from the kitchen.";
				],
		has pluralname scenery;
	
Object -> -> Sugar "sugar"
	with name 'sugar',
		description "It's just some regular sugar in a large regular sack.",
					before [;
				Take: "This sack is too large to carry around, and you don't want to take loose sugar with you.";
				Eat: "You taste the sugar. Sweet!";
				Smell: "You don't notice anything special.";
				],
		has static pluralname scenery;
		
Object -> -> Biscuits "biscuits"
	with name 'biscuit' 'biscuits',
		description "These look like something else is enjoying them already. Creepy crawly entomophobia-inducing.",
			before [;
				Eat: "You aren't going there. Something has beaten you to it.";
				Smell: "They don't smell fresh.";
				Touch: "You'd rather not. Imaging picking them up!";
				],
			after [;
				Take: if (location == kitchen) {TakenBix = 1 ; "Despite the bugs, you grab the biscuits. Your entomophobia triggers."; };
				],
		has scored pluralname concealed;

Object -> -> Biscuxxs "biscuits"
	with name 'biscuxxs' 'biscuxx',
		description "These look like something else is enjoying them already. Creepy crawly entomophobia-inducing.",
			before [;
				Eat: "You aren't going there. Something has beaten you to it.";
				Smell: "They don't smell fresh.";
				Touch: "You'd rather not. Imaging picking them up!";
				Take: "You've taken enough biscuits from the kitchen.";
				],
		has pluralname scenery;

Object PubBack "Back of the Pub"
	with
		description "You are in an overgrown area behind the back of the pub. The narrow dirt path you followed ends here. There is, as expected, a shed here. The shed door is wide open so you can go in, but
		who knows what is in there. You can head west back from where you came.",
		w_to PubSide,
		n_to PubSide;

Object -> Shed "utility shed"
		with
			name 'shed',
			description "It's an detached shed used primarily for storage such as gardening tools. The door appears to be open.",
			inside_description "You are in the small shed behind the pub. The door is stuck open.",
!				before[;
!					Enter: if ( noun == "box") {score++;};
!					],
			has scenery static enterable container open;

Object -> -> ShedDoor "shed door"
		with
			name 'door',
			description "The door hasn't been used in some time. It is well and truly stuck in position.",
			has scenery static;

Object -> -> GardenTools "gardening tools"
		with
			name 'gardening' 'tools',
			description "It's a selection of ordinary gardening tools.",
			before [;
				Take: "The gardening tools are not going to help you today.";
				],
			has scenery static;

Object -> -> Box "box"
	with
		name 'box',
		description "This is a somewhat large box, kept here out of the weather. From the outside it looks big enough that you could even comfortably fit in there, if you were so inclined.",
		inside_description [;
			print "This is a somewhat large box, kept here out of the weather. You are inside the box, which is probably not for the best as it definitely seems smaller on the inside.^";
			if (boxclaus == 0) {score++; boxclaus = 1;
			"^You realise this a box that is just a little too small for you to be comfortable inside.
				Your claustrophobia kicks in.";}; rtrue;
			],
		after [;
			Enter : if (boxclaus == 0) {score++; boxclaus = 1;
			"^You realise this a box that is just a little too small for you to be comfortable inside.
				Your claustrophobia kicks in.";}; rtrue;
			],
	has container open openable enterable static;

Object CleanGear "cleaning supplies"
	with
		name 'cleaning' 'supplies',
		description "A range of industrial strength cleaning supplies fit for the toughest of jobs. I bet it would help you clean almost anything.",
		has	pluralname;

[InputWord n wa wl i;
  wa = WordAddress(n); wl = WordLength(n);
  for(i=0 : i < wl : i++) print (char) wa->i;
];


[Initialise;
	print "^^Maybe you're tired. Whatever is happening to you, everything is making you uneasy. The tiniest things are putting you on edge and making you afraid.^^
	Perhaps the best option to get through the day is to work your way through them, triggering one at a time and^^";

];
